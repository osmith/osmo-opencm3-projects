
#include <libopencm3/stm32/gpio.h>

#include <libcommon/utils.h>

#include "attenuator.h"
#include "board.h"

static const struct attenuator_def attenuator_def_at1[NUM_STAGE] = {
	{ { GPIOB, GPIO3 } }, { { GPIOA, GPIO15 } },
};
static const struct attenuator_def attenuator_def_at2[NUM_STAGE] = {
	{ { GPIOB, GPIO5 } }, { { GPIOB, GPIO4 } },
};
static const struct attenuator_def attenuator_def_at3[NUM_STAGE] = {
	{ { GPIOD, GPIO0 } }, { { GPIOD, GPIO1 } },
};
static const struct attenuator_def attenuator_def_at4[NUM_STAGE] = {
	{ { GPIOA, GPIO0 } }, { { GPIOA, GPIO1 } },
};
static struct attenuator_state attenuator_st_at1[NUM_STAGE];
static struct attenuator_state attenuator_st_at2[NUM_STAGE];
static struct attenuator_state attenuator_st_at3[NUM_STAGE];
static struct attenuator_state attenuator_st_at4[NUM_STAGE];

/* LE definition of the Attenuator Channels, taken from schematics */
static const struct attenuator_channel_def channels[NUM_CHAN] = {
	[0] = { "AT1", ARRAY_SIZE(attenuator_def_at1), attenuator_def_at1 },
	[1] = { "AT2", ARRAY_SIZE(attenuator_def_at2), attenuator_def_at2 },
	[2] = { "AT3", ARRAY_SIZE(attenuator_def_at3), attenuator_def_at3 },
	[3] = { "AT4", ARRAY_SIZE(attenuator_def_at4), attenuator_def_at4 },
};

const struct attenuator_cfg board_att_cfg = {
	.num_channels = ARRAY_SIZE(channels),
	.channels = channels,
	.gpio_clock = { GPIOA, GPIO11 },
	.gpio_data = { GPIOA, GPIO12 },
};

struct attenuator_state *board_att_st[NUM_CHAN] = {
	[0] = attenuator_st_at1,
	[1] = attenuator_st_at2,
	[2] = attenuator_st_at3,
	[3] = attenuator_st_at4,
};
