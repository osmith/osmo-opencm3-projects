#pragma once
#include <stdint.h>

struct attenuator_gpio {
	uint32_t bank;
	uint32_t gpio_nr;
};


/* definition o the Latch Enable line for one given Attenuator */
struct attenuator_def {
	struct attenuator_gpio le;
};

/* Our board has two attenuators in series for each RF channel */
struct attenuator_channel_def {
	const char *name;
	unsigned int num_stages;
	const struct attenuator_def *stage;
};

struct attenuator_cfg {
	unsigned int num_channels;
	const struct attenuator_channel_def *channels;
	struct attenuator_gpio gpio_clock;
	struct attenuator_gpio gpio_data;
};

enum attenuator_value {
	ATT_VAL_CURRENT,
	ATT_VAL_STARTUP
};

/* dynamic [runtime] state of one attenuator chip */
struct attenuator_state {
	struct {
		uint8_t current;
		uint8_t startup;
	} value_qdB;
};

int attenuator_stage_set(uint8_t channel, uint8_t stage, uint8_t val_qdb);
int attenuator_stage_get(uint8_t channel, uint8_t stage, enum attenuator_value av);
void attenuator_init(const struct attenuator_cfg *cfg,
		     struct attenuator_state **state);

int attenuator_chan_set(uint8_t channel, uint8_t val_qdb, bool split_equal);
int attenuator_chan_get(uint8_t channel, enum attenuator_value av);
