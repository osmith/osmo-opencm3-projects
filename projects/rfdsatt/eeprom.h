#pragma once
#include <stdint.h>

int eeprom_read(uint8_t *buf_out, unsigned int num_bytes, uint16_t read_addr);
int eeprom_write_byte(uint16_t write_addr, uint8_t byte);
