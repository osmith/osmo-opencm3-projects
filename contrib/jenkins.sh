#!/bin/bash

TOPDIR=`pwd`

set -e

publish="$1"


# make sure the submodules are upt-o-date
git submodule init
git submodule update


PROJECTS="relay-driver rfdsatt "

cd $TOPDIR/projects
for proj in $PROJECTS; do
	echo "=============== $proj START  =============="
	cd "$TOPDIR/projects/$proj"
	make clean
	make
	echo "=============== $proj RES:$? =============="

	if [ "x$publish" = "x--publish" ]; then
		echo
		echo "=============== UPLOAD BUILD  =============="

		cat > "/build/known_hosts" <<EOF
[ftp.osmocom.org]:48 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDgQ9HntlpWNmh953a2Gc8NysKE4orOatVT1wQkyzhARnfYUerRuwyNr1GqMyBKdSI9amYVBXJIOUFcpV81niA7zQRUs66bpIMkE9/rHxBd81SkorEPOIS84W4vm3SZtuNqa+fADcqe88Hcb0ZdTzjKILuwi19gzrQyME2knHY71EOETe9Yow5RD2hTIpB5ecNxI0LUKDq+Ii8HfBvndPBIr0BWYDugckQ3Bocf+yn/tn2/GZieFEyFpBGF/MnLbAAfUKIdeyFRX7ufaiWWz5yKAfEhtziqdAGZaXNaLG6gkpy3EixOAy6ZXuTAk3b3Y0FUmDjhOHllbPmTOcKMry9
[ftp.osmocom.org]:48 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPdWn1kEousXuKsZ+qJEZTt/NSeASxCrUfNDW3LWtH+d8Ust7ZuKp/vuyG+5pe5pwpPOgFu7TjN+0lVjYJVXH54=
[ftp.osmocom.org]:48 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK8iivY70EiR5NiGChV39gRLjNpC8lvu1ZdHtdMw2zuX
EOF
		SSH_COMMAND="ssh -o 'UserKnownHostsFile=/build/known_hosts' -p 48"
		rsync --archive --verbose --compress --delete --rsh "$SSH_COMMAND" ${proj}.{bin,elf,srec} binaries@ftp.osmocom.org:web-files/osmo-opencm3-projects/latest/
		rsync --archive --verbose --compress --rsh "$SSH_COMMAND" ${proj}-*.{bin,elf,srec} binaries@ftp.osmocom.org:web-files/osmo-opencm3-projects/all/
	fi
done
