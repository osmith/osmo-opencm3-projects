-include $(OBJS:.o=.d)

LIBRFN_DIR = ../../librfn

ifeq ($(V),1)
$(info Using $(LIBRFN_DIR) path to librfn library)
endif

OBJS += \
	bitops.o \
	fibre.o \
	fibre_default.o \
	list.o \
	messageq.o \
	regdump.o \
	ringbuf.o \
	time_libopencm3.o \
	util.o

vpath %.c $(LIBRFN_DIR)/librfn
vpath %.c $(LIBRFN_DIR)/librfn/libopencm3

#CPPFLAGS += -DNDEBUG
#CPPFLAGS += -DCONFIG_CONSOLE_FROM_ISR=1
CPPFLAGS += -I$(LIBRFN_DIR)/include
