
GIT_VERSION:=$(shell git describe --tags)

LIBCOMMON_DIR = ../../libcommon
CPPFLAGS += -I$(LIBCOMMON_DIR)/include -D_GNU_SOURCE -DGIT_VERSION=\"$(GIT_VERSION)\"
vpath %.c $(LIBCOMMON_DIR)/src/
